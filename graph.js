const fetch = require('node-fetch');
const writeJsonFile = require('write-json-file');
(async () => {

    const res = await fetch("https://yashpointsales.myshopify.com/admin/api/2021-04/graphql.json", {
        method: "POST",
        headers: { "Content-Type": "application/json","X-Shopify-Access-Token":"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" },
        body: JSON.stringify({
            query: `
            query {
                    product {
                       variants
                    } 
                }`
        })
    })
    const data = await res.json()
    await writeJsonFile('yn.json',data)
    console.log({data});
    
})().catch(console.error);