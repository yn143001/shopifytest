const convert = (data) => {
  const type = data.variants.length > 1 ? "VARIENT" : "SIMPLE";
  const brand = data.vendor;
  const category = data.product_type;
  const varient = data.variants;
  const varientoptions = data.options;

  const product = {
    id: data.id,
    name: data.title,
    productCode: varient[0].barcode,
    description: data.body_html,
    imageUrl: data.image.src,
    categoryId: category,
    brandId: brand,
    type: type,
    costPrice: varient[0].price,
    retailPrice: varient[0].price,
    taxId: "6741cc4c-e173-433f-ac88-9fe05fccb768",
  };
  const brand = {
    name: brand,
  };
  const catagory = {
    name: category,
    tax: {
      taxRate: 18,
      name: "SGST",
    },
  };

  let sku;
  if (type === "SIMPLE") {
    sku = {
      name: data.title,
      barcode: varient[0].barcode,
      costPrice: varient[0].price,
      retailPrice: varient[0].price,
      taxId: "6741cc4c-e173-433f-ac88-9fe05fccb768",
      archived: false,
      productId: data.id,
    };
  } else {
    let skus = [];
    for (let i = 0; i < varient.length; i++) {
      const vart = varient[i];
      skus.push({
        id: vart.id,
        productId: vart.product_id,
        name: data.title + " " + vart.title,
        barcode: vart.barcode,
        costPrice: vart.price,
        retailPrice: vart.price,
        taxId: "6741cc4c-e173-433f-ac88-9fe05fccb768",
        archived: false,
        productId: data.id,
      });
    }
    const ProductSkuVariant = {
      variantAttribute: {
        name: data.options.name,
      },
      variantAttributeId,
      variantValue: data.varient.title,
    };
  }
};
